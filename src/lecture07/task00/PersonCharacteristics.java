package lecture07.task00;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();


        for (int i = 0; i < N; i++) {
            String dataLine = scanner.nextLine();
            String[] separatedData = dataLine.split(";");

            String firstName = separatedData[0];
            String lastName = separatedData[1];
            char gender = separatedData[2].charAt(0);
            int height = Integer.parseInt(separatedData[3]);
            LocalDate dateOfBirth = LocalDate.parse(separatedData[4], DateTimeFormatter.ofPattern("d.M.yyyy"));
            String institutionName = separatedData[5];
            LocalDate enrollmentDate = LocalDate.parse(separatedData[6], DateTimeFormatter.ofPattern("d.M.yyyy"));
            LocalDate graduationDate = LocalDate.parse(separatedData[7], DateTimeFormatter.ofPattern("d.M.yyyy"));
            double finalGrade = -1;
            if (separatedData.length > 8) {
                finalGrade = Double.parseDouble(separatedData[8]);
            }

            SecondaryEducation education = new SecondaryEducation(institutionName, enrollmentDate, graduationDate,
                    finalGrade);
            Person person = new Person(firstName, lastName, gender, height, dateOfBirth, education);
            person.printPerson();
        }
    }
}
