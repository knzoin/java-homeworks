package lecture07.task01;

import java.util.Scanner;

public class CalculatorClient {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String commandAndNumbers = sc.nextLine();

        while (!commandAndNumbers.equals("END")) {

            String[] separatedCommandAndNumbers = commandAndNumbers.split(" ");
            String command = separatedCommandAndNumbers[0];
            double firstNumber = Double.parseDouble(separatedCommandAndNumbers[1]);
            double secondNumber = Double.parseDouble(separatedCommandAndNumbers[2]);

            double result = 0;
            if (command.equals("SUM")) {
                result = Calculator.sum(firstNumber, secondNumber);
            } else if (command.equals("SUB")) {
                result = Calculator.subtract(firstNumber, secondNumber);
            } else if (command.equals("MUL")) {
                result = Calculator.multiply(firstNumber, secondNumber);
            } else if (command.equals("DIV")) {
                result = Calculator.divide(firstNumber, secondNumber);
            } else if (command.equals("PER")) {
                result = Calculator.percentage(firstNumber, secondNumber);
            }
            System.out.printf("%.3f%n", result);
            commandAndNumbers = sc.nextLine();
        }
    }
}
