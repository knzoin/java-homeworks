package lecture09.task02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CircleTest {

    @Test
    void calculate_area_of_circle() {

        //GIVEN
        Shape circle = new Circle(2);

        //WHEN
        double circleArea = circle.getArea();

        //THEN
        Assertions.assertEquals(12.566370614359172, circleArea);
    }
}