package lecture09.task02;

public class Rectangle extends Shape {

    double a;
    double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }
    @Override
    double getArea() {
        double rectangleArea = a * b;
        return rectangleArea;
    }
}
