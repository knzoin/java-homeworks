package lecture09.task04;

public abstract class Character {

    final String name;
    int attack;
    int health;

    public Character(String name, int attack, int health) {
        this.name = name;
        this.attack = attack;
        this.health = health;
    }

    abstract void specialAbility();

    public final boolean isAlive() {
        if (health > 0) {
            return true;
        }
        return false;
    }

    public final void healFor(int amount) {
        health += amount;
    }

    public final void increaseAttack(int amount) {
        attack += amount;
    }

    public final String getName() {
        return name;
    }

    final void fight(Character other) {

        this.specialAbility();
        other.specialAbility();

        this.health -= other.attack;
        other.health -= this.attack;
    }
}
