package lecture09.task04;

public class Battlefield {
    Character hero;
    Orc[] enemies;

    public Battlefield(Character hero, Orc[] enemies) {
        this.hero = hero;
        this.enemies = enemies;
    }

    String combat() {
        for (Orc enemy : enemies) {
            while (hero.health > 0 && enemy.health > 0) {
                hero.fight(enemy);
            }
            if (hero.health <= 0) {
                return defeatMessage(enemy.getName());
            }
        }
        return victoryMessage();
    }

    String victoryMessage() {
        return "Against all odds, " + hero.getName() + " has prevailed!";
    }

    String defeatMessage(String enemyName) {
        return enemyName
                + "has slain " + hero.getName() + "! All hope is lost.";
    }
}
