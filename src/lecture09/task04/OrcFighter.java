package lecture09.task04;

public class OrcFighter extends Orc {

    public OrcFighter(String name) {
        super(name);
    }

    @Override
    void specialAbility() {
        super.increaseAttack(1);
    }
}
