package lecture09.task04;

public abstract class Orc extends Character {

    public Orc(String name) {
        super(name, 4, 15);
    }
}
