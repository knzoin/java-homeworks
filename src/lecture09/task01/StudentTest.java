package lecture09.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StudentTest {

    @Test
    void assert_correct_fields_are_populated_for_student_info() {
// GIVEN
        String firstName = "Peter";
        String lastName = "Petrov";
        String facultyNumber = "F1234";
        Person student = new Student(firstName, lastName, facultyNumber, 3, 5);
// WHEN
        String info = student.getInfo();
// THEN
        Assertions.assertTrue(info.contains("First name: " + firstName));
        Assertions.assertTrue(info.contains("Last name: " + lastName));
        Assertions.assertTrue(info.contains("Occupation: Student"));
        Assertions.assertTrue(info.contains("Faculty number: " + facultyNumber));
        Assertions.assertFalse(info.contains("Salary per hour:"));
        Assertions.assertFalse(info.contains("Week salary"));

    }

    @Test
    void student_hours_per_week_calculated_properly() {
// GIVEN
        int lectureCount = 2;
        int exerciseCount = 4;
        Person student = new Student("Peter", "Petrov", "F1234",
                                     lectureCount, exerciseCount);
// WHEN
        String info = student.getInfo();
// THEN
        Assertions.assertTrue(info.contains("Hours per day: 2.00"));
    }
}