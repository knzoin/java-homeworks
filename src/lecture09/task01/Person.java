package lecture09.task01;

public class Person {

        String firstName;
        String lastName;

    public Person(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        String getInfo() {
            String formattedText = String.format(
                    "First name: %s%n" +
                            "Last name: %s%n", firstName, lastName);
            return formattedText;
        }
    }
