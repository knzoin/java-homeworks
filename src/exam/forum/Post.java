package exam.forum;

import java.time.LocalDateTime;

public class Post {

    private String content;
    private String postAuthor;
    private LocalDateTime postCreationTime;

    protected Post(String content, String postAuthor, LocalDateTime postCreationTime) {
        this.content = content;
        this.postAuthor = postAuthor;
        this.postCreationTime = postCreationTime;
    }

    protected boolean changePostContent(User username) {
        try {
            if (postAuthor.equals(username)) {
                return true;
            } catch(UserNotAuthorException e) {

            }
        }

    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(String postAuthor) {
        this.postAuthor = postAuthor;
    }

    public LocalDateTime getPostCreationTime() {
        return postCreationTime;
    }

    public void setPostCreationTime(LocalDateTime postCreationTime) {
        this.postCreationTime = postCreationTime;
    }
}
