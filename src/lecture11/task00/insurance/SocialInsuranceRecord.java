package lecture11.task00.insurance;

import java.time.YearMonth;
import java.util.Objects;

public class SocialInsuranceRecord {

    private double amount;
    private YearMonth yearMonth;

    public SocialInsuranceRecord(double amount, YearMonth yearMonth) {
        this.amount = amount;
        this.yearMonth = yearMonth;
    }

    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public YearMonth getYearMonth() {
        return yearMonth;
    }
    public void setYearMonth(YearMonth yearMonth) {
        this.yearMonth = yearMonth;
    }
}
