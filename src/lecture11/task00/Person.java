package lecture11.task00;

import lecture09.task00.education.Education;
import lecture09.task00.education.GradedEducation;
import lecture11.task00.insurance.SocialInsuranceRecord;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Person {

    String firstName;
    String lastName;
    char gender;
    int height;
    LocalDate dateOfBirth;
    Education education;
    List<SocialInsuranceRecord> socialInsuranceRecords;

    public Person(String firstName, String lastName, char gender, int height, LocalDate dateOfBirth, Education education) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.dateOfBirth = dateOfBirth;
        this.education = education;
        socialInsuranceRecords = new ArrayList<>();
    }

    void printPerson() {
        int age = calculateAge();
        String[] pronouns = determinePronouns();

        if (education instanceof GradedEducation) {
            GradedEducation gradedEducation = (GradedEducation) education;
            double finalGrade = gradedEducation.getFinalGrade();

            System.out.printf("%s %s is %d years old. %s was born in %d. %s started %s on %s and finished on %s with a grade of " +
                            "%.3f",
                    firstName, lastName, age, pronouns[2], dateOfBirth.getYear(), pronouns[2], education.getInstitutionName(),
                    education.getEnrollmentDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                    education.getGraduationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), finalGrade);
        } else {
            System.out.printf("%s %s is %d years old. %s was born in %d. %s started %s on %s and is supposed to " +
                            "graduate on %s.",
                    firstName, lastName, age, pronouns[2], dateOfBirth.getYear(), pronouns[2], education.getInstitutionName(),
                    education.getEnrollmentDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                    education.getGraduationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        if (age < 18) {
            System.out.printf("%s %s is under-aged.", firstName, lastName);
        }

        System.out.println();
    }

    int calculateAge() {
        int age = LocalDate.now().getYear() - dateOfBirth.getYear();
        return age;
    }

    String[] determinePronouns() {

        String[] pronouns = new String[3];

        if (gender == 'M') {
            pronouns[0] = "His";
            pronouns[1] = "he";
            pronouns[2] = "He";
        } else if (gender == 'F') {
            pronouns[0] = "Her";
            pronouns[1] = "she";
            pronouns[2] = "She";
        }
        return pronouns;
    }

    public void addInsuranceRecord(SocialInsuranceRecord record) {
        socialInsuranceRecords.add(record);
    }

    public List<SocialInsuranceRecord> getSocialInsuranceRecords() {
        return new ArrayList<>(socialInsuranceRecords);
    }
}


