package lecture11.task00.education;

import java.time.LocalDate;

public abstract class GradedEducation extends Education {
    private double finalGrade = -1;

    public GradedEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate) {
        super(institutionName, enrollmentDate, graduationDate);
    }

    public double getFinalGrade() {
        return finalGrade;
    }
    public void graduate(double finalGrade) {
        graduate();
        this.finalGrade = finalGrade;
    }

}

