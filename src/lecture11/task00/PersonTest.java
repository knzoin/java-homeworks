package lecture11.task00;

import lecture09.task00.education.PrimaryEducation;
import lecture11.task00.insurance.SocialInsuranceRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

class PersonTest {

    @Test
    void when_adding_social_insurance_record_the_record_exists() {
        //GIVEN
        Person person = new Person("Peter", "Petrov", 'M', 180,
                LocalDate.parse("7.6.1990", DateTimeFormatter.ofPattern("d.M.yyyy")),
                new PrimaryEducation("SOU Pushkin",
                        LocalDate.parse("15.9.1997", DateTimeFormatter.ofPattern("d.M.yyyy")),
                        LocalDate.parse("30.5.2002", DateTimeFormatter.ofPattern("d.M.yyyy"))));

        SocialInsuranceRecord record = new SocialInsuranceRecord(329, YearMonth.now());

        //WHEN
        person.addInsuranceRecord(record);

        //THEN
        Assertions.assertTrue(person.getSocialInsuranceRecords().contains(record));
    }

    @Test
    void when_adding_social_insurance_record_the_data_in_the_record_is_valid() {
        //GIVEN
        Person person = new Person("Peter", "Petrov", 'M', 180,
                LocalDate.parse("7.6.1990", DateTimeFormatter.ofPattern("d.M.yyyy")),
                new PrimaryEducation("SOU Pushkin",
                        LocalDate.parse("15.9.1997", DateTimeFormatter.ofPattern("d.M.yyyy")),
                        LocalDate.parse("30.5.2002", DateTimeFormatter.ofPattern("d.M.yyyy"))));
        SocialInsuranceRecord record = new SocialInsuranceRecord(329, YearMonth.now());

        //WHEN
        person.addInsuranceRecord(record);

        //THEN
        Assertions.assertEquals(YearMonth.now(), person.getSocialInsuranceRecords().get(0).getYearMonth());
        Assertions.assertEquals(329, person.getSocialInsuranceRecords().get(0).getAmount());
    }
}