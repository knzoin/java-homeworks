package lecture03.task07;

import java.util.Scanner;

public class SecondsInBiggerIntervals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter seconds: ");

        int seconds = scanner.nextInt();
        int secondsInDay = 60 * 60 * 24;
        int D = seconds / (secondsInDay);
        seconds = seconds % (secondsInDay);

        int secondsInHour = 60 * 60;
        int H = seconds / (secondsInHour);
        seconds = seconds % (secondsInHour);

        int secondsInMinute = 60;
        int M = seconds / secondsInMinute;
        seconds = seconds % secondsInMinute;

        System.out.printf("%d days, %d hours, %d minutes, %d seconds", D, H, M, seconds);
    }
}

