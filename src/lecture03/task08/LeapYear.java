package lecture03.task08;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a year and find if it is leap: ");

        int year = scanner.nextInt();

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.print(true);
                } else {
                    System.out.print(false);
                }
            } else {
                System.out.print(true);
            }
        } else {
            System.out.print(false);
        }
    }
}
