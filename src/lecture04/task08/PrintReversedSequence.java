package lecture04.task08;

import java.util.Scanner;

public class PrintReversedSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();

        String allNumbers = "";

        for (int i = 0; i < number; i++) {
            int inputNumber = scanner.nextInt();
            allNumbers += inputNumber + " ";
        }

        allNumbers = allNumbers.trim();
        char[] numbersToBeReversed = allNumbers.toCharArray();

        for (int i = numbersToBeReversed.length - 1; i >= 0; i--) {
            System.out.print(numbersToBeReversed[i]);
        }

    }
}
