package lecture04.task05;

import java.util.Scanner;

public class PrintSumOfN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sum = 0;

        for (int i = 0; i < n; i++) {
            int inputNumber = scanner.nextInt();
            sum += inputNumber;
        }
        System.out.println(sum);
    }
}
