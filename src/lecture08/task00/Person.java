package lecture08.task00;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Person {

    String firstName;
    String lastName;
    char gender;
    int height;
    LocalDate dateOfBirth;
    SecondaryEducation education;

    public Person(String firstName, String lastName, char gender, int height, LocalDate dateOfBirth, SecondaryEducation education) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.dateOfBirth = dateOfBirth;
        this.education = education;
    }

    public void printPerson() {
        int age = calculateAge();
        String[] pronouns = determinePronouns();

        if (education.getFinalGrade() != -1) {

            System.out.printf("%s %s is %d years old. %s was born in %d. %s started %s on %s and finished on %s with a grade of " +
                            "%.3f",
                    firstName, lastName, age, pronouns[2], dateOfBirth.getYear(), pronouns[2], education.getInstitutionName(),
                    education.getEnrollmentDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                    education.getGraduationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), education.getFinalGrade());
        } else {
            System.out.printf("%s %s is %d years old. %s was born in %d. %s started %s on %s and is supposed to " +
                            "graduate on %s.",
                    firstName, lastName, age, pronouns[2], dateOfBirth.getYear(), pronouns[2], education.getInstitutionName(),
                    education.getEnrollmentDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                    education.getGraduationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        if (age < 18) {
            System.out.printf("%s %s is under-aged.", firstName, lastName);
        }

        System.out.println();
    }

    int calculateAge() {
        int age = LocalDate.now().getYear() - dateOfBirth.getYear();
        return age;
    }

    String[] determinePronouns() {

        String[] pronouns = new String[3];

        if (gender == 'M') {
            pronouns[0] = "His";
            pronouns[1] = "he";
            pronouns[2] = "He";
        } else if (gender == 'F') {
            pronouns[0] = "Her";
            pronouns[1] = "she";
            pronouns[2] = "She";
        }
        return pronouns;
    }
}


