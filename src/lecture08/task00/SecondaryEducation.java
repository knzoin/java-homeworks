package lecture08.task00;

import java.time.LocalDate;

public class SecondaryEducation {
    private final String institutionName;
    private final LocalDate enrollmentDate;
    private LocalDate graduationDate;
    private double finalGrade;
    private boolean graduated;

    public SecondaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, double finalGrade) {
        this.institutionName = institutionName;
        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        setFinalGrade(finalGrade);
    }

    public void setFinalGrade(double grade) {
        if (grade > 0) {
            graduated = true;
            finalGrade = grade;
        } else {
            finalGrade = -1;
        }
    }

    public double getFinalGrade() {
        return finalGrade;
    }

    public boolean isGraduated() {
        return graduated;
    }
    public String getInstitutionName() {
        return institutionName;
    }
    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }
    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(LocalDate graduationDate) {
        this.graduationDate = graduationDate;
    }
}
