package lecture10.task00;

public class ArgumentMissingException extends Exception {
    public ArgumentMissingException(String message) {
        super(message);
    }
}
