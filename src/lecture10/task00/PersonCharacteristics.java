package lecture10.task00;

import lecture09.task00.education.HigherEducation;
import lecture09.task00.education.PrimaryEducation;
import lecture09.task00.education.SecondaryEducation;
import org.junit.platform.commons.util.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < N; i++) {
            try {
                Person person = readNewPerson(scanner);
                person.printPerson();
            } catch (IllegalArgumentException | ArgumentMissingException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private static Person readNewPerson(Scanner scanner) throws ArgumentMissingException {
        String dataLine = scanner.nextLine();
        String[] separatedData = dataLine.split(";");
        String firstName = separatedData[0];
        if (StringUtils.isBlank(firstName)) {
            throw new ArgumentMissingException("Expected non-empty first name");
        }
        String lastName = separatedData[1];
        if (StringUtils.isBlank(lastName)) {
            throw new ArgumentMissingException("Expected non-empty last name");
        }
        char gender = separatedData[2].charAt(0);
        if (gender != 'M' || gender != 'F') {
            throw new IllegalArgumentException("Expected M or F for gender.");
        }
        int height = Integer.parseInt(separatedData[3]);
        if (height < 40 || height > 300) {
            throw new IllegalArgumentException("Expected height is between 40 and 300 cm.");
        }
        LocalDate dateOfBirth = LocalDate.parse(separatedData[4], DateTimeFormatter.ofPattern("d.M.yyyy"));
        LocalDate errorDate = LocalDate.of(1900, 1, 1);
        if (dateOfBirth.isBefore(errorDate) || dateOfBirth.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Date of birth is expected to be after 01.01.1900 and before now.");
        }
        char educationType = separatedData[5].charAt(0);
        String institutionName = separatedData[6];
        if (StringUtils.isBlank(institutionName)) {
            throw new ArgumentMissingException("Expected non-empty institution name.");
        }
        LocalDate enrollmentDate = LocalDate.parse(separatedData[7], DateTimeFormatter.ofPattern("d.M.yyyy"));
        LocalDate graduationDate = LocalDate.parse(separatedData[8], DateTimeFormatter.ofPattern("d.M.yyyy"));
        if (enrollmentDate.isAfter(graduationDate)) {
            throw new IllegalArgumentException("Graduation date is expected to be after enrollment date.");
        }
        double finalGrade;
        if (separatedData.length > 9) {
            finalGrade = Double.parseDouble(separatedData[9]);
            if (graduationDate.isAfter(LocalDate.now())) {
                throw new IllegalArgumentException("Graduation date is expected to be a date in the past");
            }
            if (finalGrade < 2 || finalGrade > 6) {
                throw new IllegalArgumentException("Graduation grade is expected to be between 2 and 6.");
            }
        } else {
            throw new ArgumentMissingException("No final grade can be provided before graduation.");
        }

        Person person;
        switch (educationType) {
            case 'P':
                PrimaryEducation pEducation = new PrimaryEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, pEducation);
                break;
            case 'S':
                SecondaryEducation sEducation = new SecondaryEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, sEducation);
                sEducation.graduate(finalGrade);
                break;
            case 'B':
            case 'M':
            case 'D':
                HigherEducation hEducation = new HigherEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, hEducation);
                hEducation.graduate(finalGrade);
                break;
            default:
                throw new IllegalArgumentException("Unrecognized education code.");
        }
        return person;
    }
}


