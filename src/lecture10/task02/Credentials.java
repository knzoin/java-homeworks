package lecture10.task02;

public class Credentials {

    private final String userName;
    private final String[] oldPasswords = new String[100];

    private int lastPasswordIndex = 0;

    Credentials(String userName, String passWord) {
        this.userName = userName;
        oldPasswords[lastPasswordIndex] = passWord;
    }

    public boolean matchingOldPassword(String currentPasswordGuess) {
        return oldPasswords[lastPasswordIndex].equals(currentPasswordGuess);
    }

    public void changePassword(String currentPasswordGuess, String newPassword)
            throws PasswordMismatchException, OldPasswordConflictException {

        if (matchingOldPassword(currentPasswordGuess)) {
            validateNewPasswordDiffersFromOldOnes(newPassword);
            oldPasswords[++lastPasswordIndex] = newPassword;
        } else {
            throw new PasswordMismatchException();
        }
    }

    private void validateNewPasswordDiffersFromOldOnes(String newPassword) throws OldPasswordConflictException {
        for (int i = 0; i <= lastPasswordIndex; i++) {
            if (newPassword.equals(oldPasswords[i])) {
                int passwordsBetweenCurrentAndMatchingOldPass = lastPasswordIndex - i;
                throw new OldPasswordConflictException(passwordsBetweenCurrentAndMatchingOldPass);
            }
        }
    }
}
