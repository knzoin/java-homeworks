package lecture10.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NumberNotANumberTest {

    @Test
    void whenPassingAValidNumberString_returnTrue() {

        //GIVEN
        String number = "123";

        //WHEN
        boolean isNumber = NumberNotANumber.isNumber(number);

        //THEN
        Assertions.assertTrue(isNumber);
    }

}