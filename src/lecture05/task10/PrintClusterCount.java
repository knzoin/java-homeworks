package lecture05.task10;

import java.util.Scanner;

public class PrintClusterCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int previousNumber = scanner.nextInt();
        boolean isCluster = false;
        int clusterCounter = 0;

        for (int i = 0; i < n - 1; i++) {
            int currentNumber = scanner.nextInt();

            if (currentNumber == previousNumber) {
                isCluster = true;
            } else {
                if (isCluster) {
                    clusterCounter++;
                    isCluster = false;
                }
            }

            previousNumber = currentNumber;
        }

        if (isCluster) {
            clusterCounter++;
        }

        System.out.println(clusterCounter);
    }
}
