package lecture05.task02;

import java.util.Scanner;

public class CreateStringArray {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter 4 comma separated names: ");

        String names = sc.nextLine();
        String[] split = names.split(",");

        System.out.println(split[0]);
        System.out.println(split[1].trim());
        System.out.println(split[2].trim());
        System.out.println(split[3].trim());
    }
}
