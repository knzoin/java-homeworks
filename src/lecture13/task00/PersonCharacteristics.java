package lecture13.task00;

import lecture09.task00.education.HigherEducation;
import lecture09.task00.education.PrimaryEducation;
import lecture09.task00.education.SecondaryEducation;
import lecture10.task00.ArgumentMissingException;
import lecture11.task00.education.EducationDegree;
import lecture11.task00.insurance.SocialInsuranceRecord;
import org.junit.platform.commons.util.StringUtils;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < N; i++) {
            try {
                Person person = readNewPerson(scanner);
                List<SocialInsuranceRecord> records = readSocialInsuranceRecords(scanner);
                person.printPerson();
            } catch (IllegalArgumentException | ArgumentMissingException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private static List<SocialInsuranceRecord> readSocialInsuranceRecords(Scanner scanner) {
        String dataLine = scanner.nextLine();
        String[] split = dataLine.split(";");

        List<SocialInsuranceRecord> records = new ArrayList<>();

        for (int i = 0; i < split.length; i += 3) {

            double amount = Double.parseDouble(split[i]);
            int year = Integer.parseInt(split[i + 1]);
            int month = Integer.parseInt(split[i + 2]);

            records.add(new SocialInsuranceRecord(amount, YearMonth.of(year, month)));
        }

        return records;

    }

    private static Person readNewPerson(Scanner scanner) throws ArgumentMissingException {
        String dataLine = scanner.nextLine();
        String[] separatedData = dataLine.split(";");
        String firstName = separatedData[0];
        if (StringUtils.isBlank(firstName)) {
            throw new ArgumentMissingException("Expected non-empty first name");
        }
        String lastName = separatedData[1];
        if (StringUtils.isBlank(lastName)) {
            throw new ArgumentMissingException("Expected non-empty last name");
        }
        char gender = separatedData[2].charAt(0);
        if (gender != 'M' && gender != 'F') {
            throw new IllegalArgumentException("Expected M or F for gender.");
        }
        int height = Integer.parseInt(separatedData[3]);
        if (height < 40 || height > 300) {
            throw new IllegalArgumentException("Expected height is between 40 and 300 cm.");
        }
        LocalDate dateOfBirth = LocalDate.parse(separatedData[4], DateTimeFormatter.ofPattern("d.M.yyyy"));
        LocalDate errorDate = LocalDate.of(1900, 1, 1);
        if (dateOfBirth.isBefore(errorDate) || dateOfBirth.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Date of birth is expected to be after 01.01.1900 and before now.");
        }
        char educationType = separatedData[5].charAt(0);
        String institutionName = separatedData[6];
        if (StringUtils.isBlank(institutionName)) {
            throw new ArgumentMissingException("Expected non-empty institution name.");
        }
        LocalDate enrollmentDate = LocalDate.parse(separatedData[7], DateTimeFormatter.ofPattern("d.M.yyyy"));
        LocalDate graduationDate = LocalDate.parse(separatedData[8], DateTimeFormatter.ofPattern("d.M.yyyy"));
        if (enrollmentDate.isAfter(graduationDate)) {
            throw new IllegalArgumentException("Graduation date is expected to be after enrollment date.");
        }
        double finalGrade;
        if (separatedData.length > 9) {
            finalGrade = Double.parseDouble(separatedData[9]);
            if (graduationDate.isAfter(LocalDate.now())) {
                throw new IllegalArgumentException("Graduation date is expected to be a date in the past");
            }
            if (finalGrade < 2 || finalGrade > 6) {
                throw new IllegalArgumentException("Graduation grade is expected to be between 2 and 6.");
            }
        } else {
            throw new ArgumentMissingException("No final grade can be provided before graduation.");
        }

        Person person;
        EducationDegree educationDegree = EducationDegree.of(educationType);
        switch (educationDegree) {
            case PRIMARY:
                PrimaryEducation pEducation = new PrimaryEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, pEducation);
                break;
            case SECONDARY:
                SecondaryEducation sEducation = new SecondaryEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, sEducation);
                sEducation.graduate(finalGrade);
                break;
            case BACHELOR:
            case MASTER:
            case DOCTORATE:
                HigherEducation hEducation = new HigherEducation(institutionName, enrollmentDate, graduationDate);
                person = new Person(firstName, lastName, gender, height, dateOfBirth, hEducation);
                hEducation.graduate(finalGrade);
                break;
            default:
                throw new IllegalArgumentException("Unrecognized education code.");
        }
        return person;
    }
}


