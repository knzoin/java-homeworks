package lecture13.task00.education;

import java.time.LocalDate;

public abstract class Education {

    boolean graduated;
    private final String institutionName;
    private final LocalDate enrollmentDate;
    private LocalDate graduationDate;

    public Education(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate) {
        this.institutionName = institutionName;
        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(LocalDate graduationDate) {
        this.graduationDate = graduationDate;
    }

    public boolean hasGraduated() {
        return graduated;
    }

    void graduate() {
        this.graduated = true;
    }


}
