package lecture13.task01;

public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;


    boolean isWorkDay() {
        if (this.equals(DayOfWeek.SATURDAY) || this.equals(DayOfWeek.SUNDAY)) {
            return false;
        } else {
            return true;
        }
    }
}

class Main {
    public static void main(String[] args) {
        System.out.println(DayOfWeek.MONDAY.isWorkDay());
    }
}

