package lecture13.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayOfWeekTest {

    @Test
    void isWorkDay() {
        //GIVEN
        DayOfWeek day = DayOfWeek.MONDAY;

        //WHEN
        boolean answer = day.isWorkDay();

        //THEN
        Assertions.assertTrue(answer);
    }
    @Test
    void isNotWorkDay() {
        //GIVEN
        DayOfWeek day = DayOfWeek.SATURDAY;

        //WHEN
        boolean answer = day.isWorkDay();

        //THEN
        Assertions.assertFalse(answer);
    }

}