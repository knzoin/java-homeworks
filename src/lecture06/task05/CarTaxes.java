package lecture06.task05;

import java.util.Scanner;

public class CarTaxes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String carProperties = scanner.nextLine();
        String[] carPropertiesArray = carProperties.split(" ");

        Car inputCar = new Car(carPropertiesArray[0],
                carPropertiesArray[1],
                Integer.parseInt(carPropertiesArray[2]),
                Integer.parseInt(carPropertiesArray[3]));

        System.out.println(inputCar.eligibleTax());
    }
}
