package lecture06.task05;

public class Car {
    String brand;
    String model;
    int enginePower;
    int yearOfManufacturing;

    public Car(String brand, String model, int yearOfManufacturing, int enginePower) {
        this.brand = brand;
        this.model = model;
        this.enginePower = enginePower;
        this.yearOfManufacturing = yearOfManufacturing;
    }


    public int insuranceCategory() {
        int carAge = 2018 - yearOfManufacturing;

        if (carAge <= 8) {
            return 1;
        }
        if (8 < carAge && carAge <= 15) {
            return 2;
        }
        if (15 < carAge && carAge <= 25) {
            return 3;
        } else {
            return 4;
        }
    }

    public double eligibleTax() {
        int category = insuranceCategory();
        double taxBase = 0;
        switch (category) {
            case 1:
                taxBase = 150.00;
                break;
            case 2:
                taxBase = 200.00;
                break;
            case 3:
                taxBase = 300.00;
                break;
            case 4:
                taxBase = 500.00;
                break;
        }
        if (enginePower < 80) {
            taxBase *= 1.2;
        }
        if (enginePower > 140) {
            taxBase *= 1.45;
        }
        return taxBase;
    }
}
