package lecture06.task08;

public class Vehicle {
    String type;
    String model;
    int power;
    double fuelConsumption;
    int yearProduced;
    int licenseNo;
    int weight;
    String color = "N/A";
    static int registrationNumber = 0;

    public Vehicle(String type, String model, int power, double fuelConsumption, int yearProduced) {
        this.type = type;
        this.model = model;
        this.power = power;
        this.fuelConsumption = fuelConsumption;
        this.yearProduced = yearProduced;
        this.licenseNo = registrationNumber++;
    }

    public Vehicle(String type, String model, int power, double fuelConsumption, int yearProduced,
                   int weight, String color) {
        this.type = type;
        this.model = model;
        this.power = power;
        this.fuelConsumption = fuelConsumption;
        this.yearProduced = yearProduced;
        this.weight = weight;
        this.color = color;
        this.licenseNo = registrationNumber++;
    }

    public double calculateTripPrice(double fuelPrice, double distance) {
        double fuelConsumedForTrip = (distance / 100) * fuelConsumption;
        double tripPrice = fuelConsumedForTrip * fuelPrice;
        return tripPrice;
    }

    public double getInsurancePrice() {
        int carAge = 2018 - yearProduced;

        double typeCoefficient = 0;
        switch (type) {
            case "car":
                typeCoefficient = 1.00;
                break;
            case "suv":
                typeCoefficient = 1.12;
                break;
            case "truck":
                typeCoefficient = 1.20;
                break;
            case "motorcycle":
                typeCoefficient = 1.50;
                break;
        }
        double insurancePrice = (0.16 * power) * (1.25 * carAge) * (0.05 * fuelConsumption) * typeCoefficient;
        return insurancePrice;
    }

}
