package lecture06.task06;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class SwiftDate {

    int day;
    int month;
    int year;

    SwiftDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    boolean isLeapYear() {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    int getCentury() {
        return year / 100 + 1;
    }

    long getDaysDifference(SwiftDate other) {
        LocalDate thisDate = LocalDate.of(year, month, day);
        LocalDate otherDate = LocalDate.of(other.year, other.month, other.day);

        return Math.abs(ChronoUnit.DAYS.between(thisDate, otherDate));
    }

    String getInfo() {


        String leapYearTemplate = "";
        if (isLeapYear()) {
            leapYearTemplate = "[It is leap year.]";

        }
        int currentCentury = getCentury();
        String info = String.format("%d %d %d - %d century.%s",
                year, month, day, currentCentury, leapYearTemplate);

        return info;
    }
}
