package lecture06.task06;

import java.util.Scanner;

public class DateDifference {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        SwiftDate firsttDate = new SwiftDate(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        SwiftDate secondDate = new SwiftDate(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());

        System.out.println(firsttDate.getDaysDifference(secondDate));
        System.out.println(firsttDate.getInfo());
        System.out.println(secondDate.getInfo());
    }
}
