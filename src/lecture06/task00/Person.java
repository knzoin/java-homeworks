package lecture06.task00;

public class Person {

    String firstName;
    String lastName;
    char gender;
    int birthYear;
    double weight;
    int height;
    String occupation;
    double[] allGrades;

    public Person(String firstName, String lastName, char gender, int birthYear, double weight, int height, String occupation,
                  double[] allGrades) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthYear = birthYear;
        this.weight = weight;
        this.height = height;
        this.occupation = occupation;
        this.allGrades = allGrades;
    }

    public void printPerson() {
        int age = calculateAge();
        String[] pronouns = determinePronouns();
        String vowelOrConsonant = vowelOrConsonant();
        double averageGrade = calculateAverageGrade();

        System.out.printf("%s %s is %d years old. %s weight is %.1f kg and %s is %d cm tall.\n%s is %s %s with an average grade of" +
                        " %.3f.",
                firstName, lastName, age, pronouns[0], weight, pronouns[1], height, pronouns[2],
                vowelOrConsonant, occupation, averageGrade);
        if (age < 18) {
            System.out.printf(" %s %s is under-aged.", firstName, lastName);
        }
        System.out.println();
    }

    int calculateAge() {
        int age = 2018 - birthYear;
        return age;
    }

    String[] determinePronouns() {

        String[] pronouns = new String[3];

        if (gender == 'M') {
            pronouns[0] = "His";
            pronouns[1] = "he";
            pronouns[2] = "He";
        } else if (gender == 'F') {
            pronouns[0] = "Her";
            pronouns[1] = "she";
            pronouns[2] = "She";
        }
        return pronouns;
    }

    String vowelOrConsonant() {

        switch (occupation.charAt(0)) {
            case 'A':
            case 'a':
            case 'E':
            case 'e':
            case 'I':
            case 'i':
            case 'O':
            case 'o':
            case 'U':
            case 'u':
                return "an";
            default:
                return "a";
        }
    }

    double calculateAverageGrade() {
        return (allGrades[0] + allGrades[1] + allGrades[2] + allGrades[3]) / 4;
    }
}


